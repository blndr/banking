import unittest
from datetime import date
from unittest.mock import patch

from app.account import Account


@patch('app.account.datetime')
class AccountTest(unittest.TestCase):
    def setup_method(self, method):
        self.account = Account()

    def test_account_accepts_deposits(self, datetime):
        # given
        datetime.today.return_value = date(2014, 6, 12)
        self.account.deposit(200)

        # when
        statement = self.account.print_statement()

        # then
        assert statement == '\n' \
                            'Date        Amount      Balance     \n' \
                            '2014-06-12  +200        200         '

    def test_account_accepts_withdrawals(self, datetime):
        # given
        datetime.today.side_effect = [date(2014, 6, 12), date(2014, 6, 13)]
        self.account.deposit(200)
        self.account.withdraw(100)

        # when
        statement = self.account.print_statement()

        # then
        assert statement == '\n' \
                            'Date        Amount      Balance     \n' \
                            '2014-06-12  +200        200         \n' \
                            '2014-06-13  -100        100         '

    def test_account_can_have_a_negative_balance(self, datetime):
        # given
        datetime.today.side_effect = [date(2014, 6, 12), date(2014, 6, 13), date(2014, 6, 16)]
        self.account.deposit(200)
        self.account.withdraw(100)
        self.account.withdraw(600)

        # when
        statement = self.account.print_statement()
        print(statement)

        # then
        assert statement == '\n' \
                            'Date        Amount      Balance     \n' \
                            '2014-06-12  +200        200         \n' \
                            '2014-06-13  -100        100         \n' \
                            '2014-06-16  -600        -500        '

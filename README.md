# Banking
This is a coding kata !
(Found here => http://kata-log.rocks/banking-kata)

Your goal is to write a class Account that offers the following methods:
+ deposit(amount: int) -> None
+ withdraw(amount: int) -> None
+ printStatement() -> str

An example of such a statement would be:

```
Date         Amount    Balance
24.12.2015   +500      500
23.8.2016    -100      400
```


## Install requirements
```
. venv/bin/activate
pip install -r requirements.txt
```

## Unit test execution
```
. venv/bin/activate
pytest
```
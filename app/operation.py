from datetime import datetime

from app import tabulate


class Operation:
    def __init__(self, date: datetime.date, amount: int):
        self.date = date
        self.amount = amount
        self.balance = amount

    def __str__(self):
        return tabulate(self.formatted_date) \
               + tabulate(self.formatted_amount) \
               + tabulate(self.formatted_balance)

    def __add__(self, other):
        sum = Operation(other.date, other.amount)
        sum.balance = self.balance + other.amount
        return sum

    @property
    def formatted_amount(self):
        return ('+' if self.amount >= 0 else '') + str(self.amount)

    @property
    def formatted_date(self):
        return self.date.isoformat()

    @property
    def formatted_balance(self):
        return str(self.balance)

from datetime import datetime

from app import STATEMENT_HEADER
from app.history import History
from app.operation import Operation


class Account:
    def __init__(self):
        self.history = History()

    def deposit(self, amount: int) -> None:
        self.history.record(Operation(datetime.today(), amount))

    def withdraw(self, amount: int) -> None:
        self.history.record(Operation(datetime.today(), -amount))

    def print_statement(self) -> str:
        return STATEMENT_HEADER + self.history.statement

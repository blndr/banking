from app.operation import Operation


class History:
    def __init__(self):
        self.operations = []

    def __iter__(self):
        for operation in self.operations:
            yield operation

    def record(self, operation: Operation) -> None:
        self.operations.append(operation)

    @property
    def statement(self):
        lines, current_line = [], None
        for operation in self:
            current_line = (current_line + operation) if current_line else operation
            lines.append(str(current_line))
        return '\n'.join(lines)
